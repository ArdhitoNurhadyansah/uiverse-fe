import React from 'react';
import { FaSearch } from 'react-icons/fa';

const Navbar: React.FC = () => {
  return (
    <nav className="navbar flex items-center justify-between p-2 bg-white pl-10 ">
      {/* Left Section */}
      <div className="flex items-center space-x-2 ml-5 pl-10">
        <a className="text-xl font-medium" style={{ color: "var(--black-color)" }}>
            UiVerse
        </a>
      </div>

    {/* Center Section (Search Bar) */}
    <div className="flex items-center"
    style={{marginLeft:-40}}>
    <div className="flex items-center relative">
        <input
        type="text"
        placeholder="Search"
        className="p-2 rounded-full border border-gray-300 placeholder-search"
        style={{ width: 550, borderRadius: '50px' }}
        />
        {/* Search Icon */}
        <div className="absolute right-0 mr-2 top-1/2 transform -translate-y-1/2">
        <FaSearch
            className="h-7 w-7 cursor-pointer text-white"
            style={{
            backgroundColor: "var(--orange-color)",
            padding: "8px",
            borderRadius: "50%",
            }}
        />
        </div>
    </div>
    </div>

      {/* Right Section */}
      <div className="flex items-center">
        {/* INI NOTIFIKASI GITU */}
      </div>
    </nav>
  );
};

export default Navbar;
