import React, { useState } from 'react';
import { useRouter } from 'next/router';
import { FaHome, FaBell, FaUser } from 'react-icons/fa';
import { useUser } from '@/utils/context';
import { logout } from "@/utils/auth";
import toast from 'react-hot-toast'

interface LeftSidebarProps {
  isHome?: boolean;
  isNotif?: boolean;
  isProfile?: boolean;
}

const LeftSidebar: React.FC<LeftSidebarProps> = ({ isHome, isNotif, isProfile }) => {
  const router = useRouter();
  const { user } = useUser();


  const handleHome = async () => {
    router.push("/postingan/yell");
  };

  const handleNotifications = () => {
    router.push("/notification")
    // Add your logic for handling notifications
  };

  const handleProfile = async () => {
    router.push("/profile");
  };

  const handleLogout = async () => {
    try {
      await logout();
      localStorage.removeItem('token');
      toast.success("Berhasil logout!")
    } catch(error) {
      console.log(error);
      toast.error("Gagal logout!")
    }
    router.push("/login");
  };

  return (
    <div className="left-sidebar-container pb-20">
      <aside className="left-sidebar" style={{ backgroundColor: 'var(--orange-color)' }}>
        <div className="profile-section">
          <div className="profile-picture">
            {/* Use user.profilePicture as the src if available, otherwise use a default image */}
            <img
              src={user?.profilePicture || 'path/to/default-profile-picture.jpg'}
              alt="Profile"
            />
          </div>
          <p className="username text-latto medium">{user?.username}</p>
        </div>

        <nav className="navigation-links">
          <button onClick={handleHome} className={isHome ? "home-button active" : "home-button"}>
            <FaHome />
            <span>Home</span>
          </button>
          <button onClick={handleNotifications} className="nav-button-notif">
            <FaBell />
            <span>Notifications</span>
          </button>
          <button onClick={handleProfile} className={isProfile ? "profile-button active" : "profile-button"}>
            <FaUser />
            <span>Profile</span>
          </button>
        </nav>

        <div onClick={handleLogout} className="logout-button" style={{ marginTop: 'auto' }}>
          LOGOUT
        </div>
      </aside>
    </div>
  );
};

export default LeftSidebar;
