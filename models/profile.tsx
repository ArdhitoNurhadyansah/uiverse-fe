interface Pengguna {
    id: string;
    npm: string;
    nama: string;
    email: string;
  }
  
  interface Profile {
    fakultas: string;
    jurusan: string;
    angkatan: string;
    program: string;
    tentang_saya: string;
    profile_picture: string | null;
  }
  
  interface UserProfile {
    role: string;
    pengguna: Pengguna;
    totalFollowers: number;
    totalUpvote: number;
    totalDownvote: number;
    profile: Profile;
  }
  
  export default UserProfile;
  