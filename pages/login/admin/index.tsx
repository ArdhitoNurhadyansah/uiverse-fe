import React from "react";
import { useState } from "react";
import { useRouter } from "next/router";
import { loginAdmin } from "@/utils/auth";
export default function LoginPage() {
  const router = useRouter();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const [passwordVisible, setPasswordVisible] = useState(false);
  const [passwordError, setPasswordError] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  const handleSubmitLogin = async () => {
    setIsLoading(true);
    try {
      const res = await loginAdmin(username, password);
      localStorage.setItem('token', res.token);
      localStorage.setItem('role', res.role);
      setTimeout(() => {
        setIsLoading(false);
      }, 2000);
      router.push("/reported/postingan");
    } catch (error: any) {
      setIsLoading(false);
      setPasswordError("Incorrect Username or Password");
    }
  };

  const handleMahasiswaLogin = () => {
    router.push("/login");
  };

  const togglePasswordVisibility = () => {
    setPasswordVisible(!passwordVisible);
  };

  return (
    <div className="flex items-center justify-center h-screen p-4 bg-gray-100">
      <div
        className="flex flex-col items-center space-y-7 p-4"
      >
        <p className="font-semibold text-poppins"
         style={{ color: "var(--black-color)", fontSize: 50}}>
          UIVerse
        </p>
        <p className="font-semibold text-poppins"
         style={{ color: "var(--black-color)", fontSize: 20, margin:-2}}>
          Welcome Back
        </p>
        <div className="space-y-4 w-full">
          <div className="relative rounded-lg">
            <input
              type="text"
              placeholder="Username"
              className="w-full px-4 py-2 border border-gray-300 rounded-lg placeholder-text"
              value={username}
              style={{width:270, height:45}}
              onChange={(e) => setUsername(e.target.value)}
            />
          </div>
          <div className="relative rounded-lg">
            <input
              type={passwordVisible ? "text" : "password"}
              placeholder="Password"
              className="w-full px-4 py-2 border border-gray-300 rounded-lg placeholder-text"
              value={password}
              style={{ width: 270, height: 45 }}
              onChange={(e) => setPassword(e.target.value)}
            />
            <span
              className="absolute right-4 top-1/2 transform -translate-y-1/2 cursor-pointer"
              onClick={togglePasswordVisibility}
            >
              {passwordVisible ? "🙈" : "👁️"}
            </span>
          </div>
          {passwordError && (
            <p className="text-red-500 text-sm text-center">{passwordError}</p>
          )}
        </div>
        <button
          className="text-white rounded-lg font-semibold text-poppins"
          style={{ backgroundColor: "var(--orange-color)", color: "var(--black-color)", width:270, height:35, fontSize: 12}}
          onClick={handleSubmitLogin}
        >
          LOGIN
        </button>
        <p className="text-poppins reguler text-sm cursor-pointer underline" onClick={handleMahasiswaLogin}
         style={{ fontSize: 11}}>
          Login sebagai Mahasiswa
        </p>
      </div>
    </div>
  );
}
