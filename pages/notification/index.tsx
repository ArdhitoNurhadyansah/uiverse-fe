// pages/index.tsx
/* eslint-disable react-hooks/rules-of-hooks */
import { useEffect, useState } from "react";
import { getAllNotifications } from "@/utils/notifications"; // Import the API functions

interface Notification {
  id: number;
  finalMessage: string;
  // Define other properties of Notification if applicable
}

const NotificationItem: React.FC<{ notification: Notification }> = ({
  notification,
}) => (
  <div>
    <p>{notification.finalMessage}</p>
  </div>
);

const IndexPage: React.FC = () => {
  const [notifications, setNotifications] = useState<Notification[]>([]);

  useEffect(() => {
    const fetchNotifications = async () => {
      try {
        const data = await getAllNotifications();
        setNotifications(data);
      } catch (error) {
        console.error("Error fetching notifications:", error);
      }
    };

    fetchNotifications();
  }, []);

  return (
    <div>
      <h1>Notifications</h1>
      {notifications.map((notification) => (
        <NotificationItem key={notification.id} notification={notification} />
      ))}
    </div>
  );
};

export default IndexPage;
