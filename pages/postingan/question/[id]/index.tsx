import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Navbar from '@/components/Navbar';  
import Sidebar from "@/components/Sidebar";
import { FaThumbsUp, FaThumbsDown, FaEdit } from 'react-icons/fa';
import { MdMessage, MdDelete } from 'react-icons/md';
import { IoMdArrowBack } from 'react-icons/io';
import { deletePostingan, getQuestion, updatePostingan, votePostingan, getVote, followQuestion, getAllAnswers, createAnswer } from "@/utils/postingan";
import { getCurrentUser } from "@/utils/profile";
import toast from 'react-hot-toast'

interface Pengguna {
  id: string;
  npm: string;
  nama: string;
  kd_org: string;
  email: string;
}

interface Mahasiswa {
  role: string;
  totalFollowers: number;
  totalUpvote: number;
  totalDownvote: number;
  pengguna: Pengguna;
}

interface Kategori {
  id: number;
  name: string;
}

interface question{
  id: string;
  content: string;
  totalUpvote: number;
  totalDownvote: number;
  createdAt: string; 
  mahasiswa: Mahasiswa;
  kategori: Kategori;
  is_taken_down: boolean;
  totalFollowers: number,
}

interface Question {
  question: question,
  is_upvote: boolean,
  is_downvote: boolean,
  is_mine: boolean,
  is_followed: boolean
}

interface Vote {
  id: string;
  voter: Mahasiswa;
  postingan: Question;
  tanggapain: null;
  is_upvote: boolean;
  is_downvote: boolean;
}



interface Answer {
  id: string;
  postingan: Question;
  mahasiswa: Mahasiswa;
  content: string;
  timestamp: string;
  is_taken_down: boolean;
}
export default function FetchDetailQuestion() {
  const [Follow, setFollow] = useState("Follow");
  const [Question, setQuestion] = useState<Question | null>(null);
  const [Mahasiswa, setMahasiswa] = useState<Mahasiswa | null>(null);
  const [Vote, setVote] = useState<Vote | null>(null);
  const [isMine, setIsMine] = useState(false)
  const [idQuestion, setIdQuestion] = useState("");
  const router = useRouter();
  const { id } = router.query; 
  const stringId: string = id as string
  const [updateQuestionId, setUpdateQuestionId] = useState("");
  const [totalAnswer, setTotalAnswer] = useState(0)
  const [Answer, setAnswer] = useState<Answer[]>();
  const [updateFormValues, setUpdateFormValues] = useState({
      content: "",
      kategori: 1,
  });
  const [createFormValues, setCreateFormValues] = useState({
    content: "",
    id: "", 
  });
  const [isUpVote, setIsUpVote] = useState(false)
  const [isDownVote, setIsDownVote] = useState(false)

  useEffect(() => {
    getQuestionById();
    statusVote(idQuestion);
  }, [id]);

  const getQuestionById = async () => {
    try {
      if (id && typeof id === "string") {
        const questionData = await getQuestion(id);
        setIdQuestion(id);
        setQuestion(questionData);
        console.log(questionData)
        console.log(id)
        await getAnswers(id);
        // getMahasiswa();
      }
    } catch (e: any) {
      console.log(e);
    }
  };

  const getAnswers = async (id:string) => {
    try {
      const answers = await getAllAnswers(id);
      setTotalAnswer(answers.length)
      setAnswer(answers)
    } catch (e: any) {
      console.log(e);
    }
  };

  const statusVote = async (id: string) => {
    try {
      const vote =  await getVote(id);
      setVote(vote);
      
      if (Vote ) {
        setIsUpVote(Vote.is_upvote);
        setIsDownVote(Vote.is_downvote)
      } 
      
    } catch (e: any) {
      console.log(e);
    }
  };

  // Delete
  const handleDelete = async (id: string) => {
    try {
      await deletePostingan(id);
      console.log(`Question dengan ID ${id} berhasil dihapus`);
      toast.success("Question berhasil di Delete!")
      router.push("/postingan/question");
    } catch (error) {
      console.log(`Gagal menghapus Question dengan ID ${id}:`, error);
      toast.error("Question gagal di Delete:(")
    }
  };

  // Follow
  const handleFollow = async (id: string) => {
    try {
      var resp = await followQuestion(id);
      console.log(`Question dengan ID ${id} berhasil difollow`);
      toast.success("Question berhasil di follow!")
      setFollow("Followed")
    } catch (error) {
      console.log(`Gagal mem-follow Question dengan ID ${id}:`, error);
      toast.error("Question gagal di follow :(")
    }
  };

  // Update
  const handleOpenUpdateModal = () => {
    if (Question != null) {
      setUpdateFormValues({
        content: Question.question.content,
        kategori: Question.question.kategori.id, 
      });
      setUpdateQuestionId(Question.question.id);
    }
  };

  const handleUpdateContentChange: React.ChangeEventHandler<HTMLInputElement> = (e) => {
    setUpdateFormValues({
      ...updateFormValues,
      content: e.target.value,
    });
  };

  const handleReplyContentChange: React.ChangeEventHandler<HTMLInputElement> = (e) => {
    setCreateFormValues({
      ...createFormValues,
      content: e.target.value,
    });
  };

  const handleUpdateKategoriChange: React.ChangeEventHandler<HTMLSelectElement> = (e) => {
    setUpdateFormValues({
      ...updateFormValues,
      kategori: parseInt(e.target.value, 10),
    });
  };

  const handleUpdate = async () => {
    if (updateFormValues.content == "") {
      toast.error('Question tidak boleh kosong!')
      console.log("ga boleh kosong")
      getQuestionById();
      setUpdateQuestionId("");
  } else {
      try {
        await updatePostingan(
          updateQuestionId,
          updateFormValues.content,
          updateFormValues.kategori
        );
        console.log(`Question dengan ID ${updateQuestionId} berhasil diperbarui`);
        toast.success("Question berhasil di update!")
      } catch (error) {
        console.log(`Gagal memperbarui Question dengan ID ${updateQuestionId}:`, error);
        toast.error("Question gagal di update:(")
      } finally {
        getQuestionById();
        setUpdateQuestionId("");
      }
    }
  };

  // Vote
  const handleUpvote = async (id: string) => {
    try {
      await votePostingan(id, 'upvote');
      setIsUpVote(true);
      setIsDownVote(false);
      getQuestionById();
      toast.success("Anda berhasil UpVote!")
      console.log(`Upvote berhasil untuk Question dengan ID ${id}`);
    } catch (error) {
      toast.error("Anda gagal UpVote:(")
      console.log(`Gagal melakukan upvote untuk Question dengan ID ${id}:`, error);
    }
  };

  const handleDownvote = async (id: string) => {
    try {
      await votePostingan(id, 'downvote');
      setIsUpVote(false);
      setIsDownVote(true);
      getQuestionById();
      toast.success("Anda berhasil DownVote!")
      console.log(`Downvote berhasil untuk Question dengan ID ${id}`);
    } catch (error) {
      toast.error("Anda gagal DownVote:(")
      console.log(`Gagal melakukan downvote untuk Question dengan ID ${id}:`, error);
    }
  };

  const handleCreateAnswer = async () => {
    if (createFormValues.content === "") {
      toast.error('Reply Question tidak boleh kosong!');
      getQuestionById();
      setCreateFormValues({
        content: "",
        id: stringId, // id yell
      });
    } else {
      try {
        await createAnswer(
          createFormValues.content,
          stringId, 
        );
        console.log('Reply Question berhasil dibuat');
        toast.success('Reply Question berhasil di post!');
      } catch (error) {
        console.log(stringId + ":nih");
        console.log('Gagal membuat Reply Yell:', error);
        toast.error('Reply Question gagal di post:(');
      } finally {
        getQuestionById();
        setCreateFormValues({
          content: "",
          id: stringId, // id yell
        });
      }
    }
  };
 
  return (
    <div className="flex flex-col">
      {/* Navbar */}
      <Navbar />

      <div className="flex">
        {/* Sidebar */}
        <Sidebar isHome={true}/>

        {/* Content */}
        <div className="content flex flex-col w-full p-4"
        
          style={{marginTop:65}}>
            <button onClick={() => window.history.back()} className="">
              <IoMdArrowBack className="rounded-xl  text-3xl"/>
            </button>
          <div className="yell-container bg-white p-4 mb-4 flex flex-col">
          <div key={Question?.question.id} className="yell-container bg-white p-4 mb-4 flex flex-col">
                {/* Row 1: ProfilePhoto, Name, Username, Kategori */}
                <div className="flex items-center mb-2">
                    <div className="profile-yell bg-black rounded-full h-7 w-7 mr-2"></div>
                    <div className="flex-grow">
                        <p className="font-bold">{Question?.question.mahasiswa.pengguna.nama} 
                            <span className="nama-yell text-latto"> - @{Question?.question.mahasiswa.pengguna.npm} </span>

                            {/* Update Question */}
                            {Question?.is_mine && (
                            <button onClick={() => handleOpenUpdateModal()} className="icon-button ml-3">
                                <FaEdit className="icon-edit"/>
                            </button>
                            )}

                            {/* Delete Question */}
                            {Question?.is_mine && (
                              <button onClick={() => handleDelete(idQuestion)} className="icon-button ml-3">
                                  <MdDelete className="icon-delete"/>
                              </button>
                            )}

                            <button  onClick={() => handleFollow(idQuestion)} className="bg-yellow-300 text-gray-500 font-medium mx-2 px-2 rounded-md">
                            {Question?.is_followed ? (
                              <p>Followed</p>
                            ) : (
                              <p>Follow</p>
                            )}
                            </button>
                            
                            {/* Update Form Modal */}
                            {updateQuestionId && (
                            <div className="fixed top-0 left-0 w-full h-full bg-gray-300 bg-opacity-50 flex items-center justify-center">
                            <div className="modal-update  bg-white p-4 rounded-md">
                                <input
                                    type="text"
                                    id="updateContent"
                                    name="content"
                                    value={updateFormValues.content}
                                    onChange={handleUpdateContentChange}
                                    className="input-update mb-2 p-2 border rounded-md w-full"
                                />
                                <div className="grid grid-cols-3 gap-4">
                                <select
                                    id="updateKategori"
                                    name="kategori"
                                    value={updateFormValues.kategori.toString()}
                                    onChange={handleUpdateKategoriChange}
                                    className="update-kat mb-2 p-2 border rounded-md w-full"
                                    >
                                    <option value={1}>Akademis</option>
                                    <option value={2}>Kantin</option>
                                    <option value={3}>Event</option>
                                    <option value={4}>Recruitment</option>
                                    <option value={5}>Fasilitas</option>
                                    <option value={6}>Out of Topic</option>
                                </select>
                                <button onClick={handleUpdate} className="btn-update text-white p-2 rounded-md mr-2">
                                Update
                                </button>
                                <button  onClick={() => setUpdateQuestionId("")} className="bg-gray-500 text-white p-2 rounded-md">
                                Cancel
                                </button>
                                </div>
                            </div>
                            </div>
                        )}
                        </p>
                    </div>
                    <div className="text-end">
                        <p className="kategori-yell">{Question?.question.kategori.name}</p>
                    </div>
                </div>

                {/* Row 2: Content */}
                <div className="mb-2 pl-9">
                <p>{Question?.question.content}</p>
                                </div>

                {/* Row 3: Upvote/Downvote/Message Icons and Reply Button */}
                <div className="flex justify-between items-center pl-9">
            
                    <div className="flex items-center">
                        {/* Upvote */}
                        <button onClick={() => handleUpvote(idQuestion)} >
                          <FaThumbsUp
                            className="icon-thumb-up h-4 w-4 mr-2"
                            style={{ color: isUpVote ? 'var(--orange-color)' : 'var(--black-color)' }}
                          />
                        </button>
                        <p>{Question?.question.totalUpvote}</p>

                        {/* Downvote */}
                        <button onClick={() => handleDownvote(idQuestion)} >
                          <FaThumbsDown
                            className="icon-thumb-down h-4 w-4 ml-4 mr-2"
                            style={{ color: isDownVote ? 'var(--orange-color)' : 'var(--black-color)' }}
                          />
                        </button>
                        <p>{Question?.question.totalDownvote}</p>

                        {/* Message  */}
                        <MdMessage className="icon-message h-4 w-4 ml-4 mr-2" />
                        <p>{totalAnswer}</p>
                        <div className="mx-5 px-2 bg-gray-200 rounded-md">
                          <p>{Question?.question.totalFollowers} followers</p>
                        </div>
                        
                        
                </div>
                <p style={{fontSize:10, textAlign:'end'}}>{Question?.question.createdAt}</p>
                </div>
          </div>

            {/* Line */}
            <div className="line-detail" />
          
            {/* Create Reply */}
            <div className="post-container bg-white p-4">

                {/* Input Text Box */}
                <input
                type="text"
                id="createContent"
                name="content"
                
                value={createFormValues.content}
                onChange={handleReplyContentChange}

                className="input-box border-none p-2 mb-4"
                placeholder="What’s on your mind?..."
                />
                <div className="flex justify-end items-end">
                <button  onClick={handleCreateAnswer} className="rounded-md ml-10 text-black py-2 px-2 bg-yellow-300">
                    Answer
                </button>
                </div>
            </div>
            
            {/* Line */}
            <div className="line-detail" />

            {/* List Reply */}
            {Answer?.map((answer, i) => (
              <div key={answer.id} className="yell-container bg-white p-4 mb-4 flex flex-col">
                {/* Row 1: ProfilePhoto, Name, Username, Kategori */}
                <div className="flex items-center mb-2">
                    <div className="profile-yell bg-black rounded-full h-7 w-7 mr-2"></div>
                    <div className="flex-grow">
                        <p className="font-bold">{answer.mahasiswa.pengguna.nama} 
                            <span className="nama-yell text-latto"> - @{answer.mahasiswa.pengguna.npm} </span>
                        </p>
                    </div>
                </div>
                {/* Row 2: Content */}
                <div className="mb-2 pl-9">
                <p>{answer.content}</p>
                <p
                style={{fontSize:10, textAlign:'end'}}>{answer.timestamp}</p>
                </div>
              </div>
            ))}


          </div>
        </div>
      </div>
    </div>
  );
}
