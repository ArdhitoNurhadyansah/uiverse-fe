import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Navbar from '@/components/Navbar';  
import Sidebar from "@/components/Sidebar";
import { FaThumbsUp, FaThumbsDown, FaEdit } from 'react-icons/fa';
import { MdMessage, MdDelete } from 'react-icons/md';
import { deletePostingan, getYell, updatePostingan, votePostingan, getVote, createReply } from "@/utils/postingan";
import { getCurrentUser } from "@/utils/profile";
import { IoMdArrowBack } from 'react-icons/io';
import toast from 'react-hot-toast'

interface Pengguna {
  id: string;
  npm: string;
  nama: string;
  kd_org: string;
  email: string;
}

interface Mahasiswa {
  role: string;
  totalFollowers: number;
  totalUpvote: number;
  totalDownvote: number;
  pengguna: Pengguna;
}

interface Kategori {
  id: number;
  name: string;
}

interface Yell {
  id: any;
  content: string;
  totalUpvote: number;
  totalDownvote: number;
  createdAt: string; 
  mahasiswa: Mahasiswa;
  kategori: Kategori;
  is_taken_down: boolean;
}

interface Reply {
  id: string;
  postingan: Yell;
  mahasiswa: Mahasiswa;
  content: string;
  timestamp: string;
  is_taken_down: boolean;
}

export default function FetchDetailYell() {
  const [Yell, setYell] = useState<Yell | null>(null);
  const [isMine, setIsMine] = useState(false)
  const [idYell, setIdYell] = useState("");
  const router = useRouter();
  const { id } = router.query; 
  const stringId: string = id as string
  const [updateYellId, setUpdateYellId] = useState("");
  const [updateFormValues, setUpdateFormValues] = useState({
      content: "",
      kategori: 1,
  });
  const [createFormValues, setCreateFormValues] = useState({
    content: "",
    id: "", 
  });
  const [isUpVote, setIsUpVote] = useState(false)
  const [isDownVote, setIsDownVote] = useState(false)
  const [totalReply, setTotalReply] = useState(0)
  const [Replies, setReplies] = useState<Reply[]>();

  useEffect(() => {
    getYellById();
  }, [id]);

  const getYellById = async () => {
    try {
      if (id && typeof id === "string") {
        const yellData = await getYell(id);
        console.log("yell", yellData)
        setYell(yellData.yell)
        setIdYell(id)
        setIsUpVote(yellData.is_upvote)
        setIsDownVote(yellData.is_downvote)
        setIsMine(yellData.is_mine)
        setTotalReply(yellData.total_reply)
        setReplies(yellData.reply)
      }
    } catch (e: any) {
      console.log(e);
    }
  };

  // Delete
  const handleDelete = async (id: string) => {
    try {
      await deletePostingan(id);
      console.log(`Yell dengan ID ${id} berhasil dihapus`);
      toast.success("Yell berhasil di Delete!")
      router.push("/postingan/yell");
    } catch (error) {
      console.log(`Gagal menghapus Yell dengan ID ${id}:`, error);
      toast.error("Yell gagal di Delete:(")
    }
  };

  // Update
  const handleOpenUpdateModal = () => {
    if (Yell != null) {
      setUpdateFormValues({
        content: Yell.content,
        kategori: Yell.kategori.id, 
      });
      setUpdateYellId(Yell.id);
    }
  };

  const handleUpdateContentChange: React.ChangeEventHandler<HTMLInputElement> = (e) => {
    setUpdateFormValues({
      ...updateFormValues,
      content: e.target.value,
    });
  };

  const handleReplyContentChange: React.ChangeEventHandler<HTMLInputElement> = (e) => {
    setCreateFormValues({
      ...createFormValues,
      content: e.target.value,
    });
  };


  const handleUpdateKategoriChange: React.ChangeEventHandler<HTMLSelectElement> = (e) => {
    setUpdateFormValues({
      ...updateFormValues,
      kategori: parseInt(e.target.value, 10),
    });
  };

  const handleUpdate = async () => {
    if (updateFormValues.content == "") {
      toast.error('Yell tidak boleh kosong!')
      console.log("ga boleh kosong")
      getYellById();
      setUpdateYellId("");
  } else {
      try {
        await updatePostingan(
          updateYellId,
          updateFormValues.content,
          updateFormValues.kategori
        );
        console.log(`Yell dengan ID ${updateYellId} berhasil diperbarui`);
        toast.success("Yell berhasil di update!")
      } catch (error) {
        console.log(`Gagal memperbarui Yell dengan ID ${updateYellId}:`, error);
        toast.error("Yell gagal di update:(")
      } finally {
        getYellById();
        setUpdateYellId("");
      }
    }
  };

  // Vote
  const handleUpvote = async (id: string) => {
    try {
      await votePostingan(id, 'upvote');
      // setIsUpVote(true);
      // setIsDownVote(false);
      getYellById();
      toast.success("Anda berhasil UpVote!")
      console.log(`Upvote berhasil untuk Yell dengan ID ${id}`);
    } catch (error) {
      toast.error("Anda gagal UpVote:(")
      console.log(`Gagal melakukan upvote untuk Yell dengan ID ${id}:`, error);
    }
  };

  const handleDownvote = async (id: string) => {
    try {
      await votePostingan(id, 'downvote');
      // setIsUpVote(false);
      // setIsDownVote(true);
      getYellById();
      toast.success("Anda berhasil DownVote!")
      console.log(`Downvote berhasil untuk Yell dengan ID ${id}`);
    } catch (error) {
      toast.error("Anda gagal DownVote:(")
      console.log(`Gagal melakukan downvote untuk Yell dengan ID ${id}:`, error);
    }
  };
  const handleCreateReply = async () => {
    if (createFormValues.content === "") {
      toast.error('Reply Yell tidak boleh kosong!');
      console.log("ga boleh kosong");
      getYellById();
      setCreateFormValues({
        content: "",
        id: stringId, // id yell
      });
    } else {
      try {
        await createReply(
          createFormValues.content,
          stringId, 
        );
        console.log('Reply Yell berhasil dibuat');
        toast.success('Reply Yell berhasil di post!');
      } catch (error) {
        console.log(stringId + ":nih");
        console.log('Gagal membuat Reply Yell:', error);
        toast.error('Reply Yell gagal di post:(');
      } finally {
        getYellById();
        setCreateFormValues({
          content: "",
          id: stringId, // id yell
        });
      }
    }
  };
  
 
  return (
    <div className="flex flex-col">
      {/* Navbar */}
      <Navbar />

      <div className="flex">
        {/* Sidebar */}
        <Sidebar isHome={true}/>

        {/* Content */}
        <div className="content flex flex-col w-full p-4"
          style={{marginTop:65}}>
             <button onClick={() => window.history.back()} className="">
              <IoMdArrowBack className="rounded-xl  text-3xl"/>
            </button>
          <div className="yell-container bg-white p-4 mb-4 flex flex-col">
          <div key={Yell?.id} className="yell-container bg-white p-4 mb-4 flex flex-col">
                {/* Row 1: ProfilePhoto, Name, Username, Kategori */}
                <div className="flex items-center mb-2">
                    <div className="profile-yell bg-black rounded-full h-7 w-7 mr-2"></div>
                    <div className="flex-grow">
                        <p className="font-bold">{Yell?.mahasiswa.pengguna.nama} 
                            <span className="nama-yell text-latto"> - @{Yell?.mahasiswa.pengguna.npm} </span>

                            {/* Update Yell */}
                            {isMine && (
                            <button onClick={() => handleOpenUpdateModal()} className="icon-button ml-3">
                                <FaEdit className="icon-edit"/>
                            </button>
                            )}

                            {/* Delete Yell */}
                            {isMine && (
                              <button onClick={() => handleDelete(idYell)} className="icon-button ml-3">
                                  <MdDelete className="icon-delete"/>
                              </button>
                            )}
                            
                            {/* Update Form Modal */}
                            {updateYellId && (
                            <div className="fixed top-0 left-0 w-full h-full bg-gray-300 bg-opacity-50 flex items-center justify-center">
                            <div className="modal-update  bg-white p-4 rounded-md">
                                <input
                                    type="text"
                                    id="updateContent"
                                    name="content"
                                    value={updateFormValues.content}
                                    onChange={handleUpdateContentChange}
                                    className="input-update mb-2 p-2 border rounded-md w-full"
                                />
                                <div className="grid grid-cols-3 gap-4">
                                <select
                                    id="updateKategori"
                                    name="kategori"
                                    value={updateFormValues.kategori.toString()}
                                    onChange={handleUpdateKategoriChange}
                                    className="update-kat mb-2 p-2 border rounded-md w-full"
                                    >
                                    <option value={1}>Akademis</option>
                                    <option value={2}>Kantin</option>
                                    <option value={3}>Event</option>
                                    <option value={4}>Recruitment</option>
                                    <option value={5}>Fasilitas</option>
                                    <option value={6}>Out of Topic</option>
                                </select>
                                <button onClick={handleUpdate} className="btn-update text-white p-2 rounded-md mr-2">
                                Update
                                </button>
                                <button  onClick={() => setUpdateYellId("")} className="bg-gray-500 text-white p-2 rounded-md">
                                Cancel
                                </button>
                                </div>
                            </div>
                            </div>
                        )}
                        </p>
                    </div>
                    <div className="text-end">
                        <p className="kategori-yell">{Yell?.kategori.name}</p>
                    </div>
                </div>

                {/* Row 2: Content */}
                <div className="mb-2 pl-9">
                <p>{Yell?.content}</p>
                </div>

                {/* Row 3: Upvote/Downvote/Message Icons and Reply Button */}
                <div className="flex justify-between items-center pl-9">
            
                    <div className="flex items-center">
                        {/* Upvote */}
                        <button onClick={() => handleUpvote(idYell)} >
                          <FaThumbsUp
                            className="icon-thumb-up h-4 w-4 mr-2"
                            style={{ color: isUpVote ? 'var(--orange-color)' : 'var(--black-color)' }}
                          />
                        </button>
                        <p>{Yell?.totalUpvote}</p>

                        {/* Downvote */}
                        <button onClick={() => handleDownvote(idYell)} >
                          <FaThumbsDown
                            className="icon-thumb-down h-4 w-4 ml-4 mr-2"
                            style={{ color: isDownVote ? 'var(--orange-color)' : 'var(--black-color)' }}
                          />
                        </button>
                        <p>{Yell?.totalDownvote}</p>

                        {/* Message  */}
                        <MdMessage className="icon-message h-4 w-4 ml-4 mr-2" />
                        <p>{totalReply}</p>
                </div>
                <p
                        style={{fontSize:10, textAlign:'end'}}>{Yell?.createdAt}</p>
                </div>
          </div>

            {/* Line */}
            <div className="line-detail" />
          
            {/* Create Reply */}
            <div className="post-container bg-white p-4">

                {/* Input Text Box */}
                <input
                type="text"
                id="createContent"
                name="content"
                
                value={createFormValues.content}
                onChange={handleReplyContentChange}

                className="input-box border-none p-2 mb-4"
                placeholder="What’s on your mind?..."
                />

                {/* Grid for Dropdown and Button */}
                <div className="grid grid-cols-2 gap-4">
                <p></p>
                {/* Button in Second Column */} 
                <button  onClick={handleCreateReply} className="post-button text-black p-2">
                    Reply
                </button>
              </div>
            </div>
            

            {/* List Reply */}
            {Replies?.map((reply, i) => (
              <div key={reply.id} className="yell-container bg-white p-4 mb-4 flex flex-col">
                {/* Row 1: ProfilePhoto, Name, Username, Kategori */}
                <div className="flex items-center mb-2">
                    <div className="profile-yell bg-black rounded-full h-7 w-7 mr-2"></div>
                    <div className="flex-grow">
                        <p className="font-bold">{reply.mahasiswa.pengguna.nama} 
                            <span className="nama-yell text-latto"> - @{reply.mahasiswa.pengguna.npm} </span>
                        </p>
                    </div>
                </div>
                {/* Row 2: Content */}
                <div className="mb-2 pl-9">
                <p>{reply.content}</p>
                <p
                style={{fontSize:10, textAlign:'end'}}>{reply.timestamp}</p>
                </div>
              </div>
            ))}

          </div>
        </div>
      </div>
    </div>
  );
}