import React, { useEffect, useState } from "react";
import { getAllYell, votePostingan, createPostingan, getYellByKategori } from "@/utils/postingan";
import Navbar from "@/components/Navbar";
import Sidebar from "@/components/Sidebar";
import Link from "next/link";
import toast from "react-hot-toast";
import { FaThumbsUp, FaThumbsDown, FaEdit } from "react-icons/fa";
import { MdMessage } from "react-icons/md";
import { useRouter } from "next/router";
import { getCauseData, reportPostingan } from "@/utils/report";

interface Pengguna {
  id: string;
  npm: string;
  nama: string;
  kd_org: string;
  email: string;
}

interface Mahasiswa {
  role: string;
  totalFollowers: number;
  totalUpvote: number;
  totalDownvote: number;
  pengguna: Pengguna;
}

interface Kategori {
  id: number;
  name: string;
}

interface Yell {
  id: string;
  content: string;
  totalUpvote: number;
  totalDownvote: number;
  createdAt: string;
  mahasiswa: Mahasiswa;
  kategori: Kategori;
  is_taken_down: boolean;
}

export default function FetchYell() {
  const router = useRouter();
  const [Yells, setYell] = useState<Yell[]>();
  const [createFormValues, setCreateFormValues] = useState({
    content: "",
    kategori: 1,
  });

  const [isReportModalOpen, setIsReportModalOpen] = useState(false);
  const [causeTitles, setCauseTitles] = useState<
    { id: string; title: string }[]
  >([]);

  const [currentStep, setCurrentStep] = useState(1);

  const [reportCause, setReportCause] = useState("");
  const [reportElaboration, setReportElaboration] = useState("");

  const [isFilterModalOpen, setIsFilterModalOpen] = useState(false);
  const [selectedFilterKategori, setSelectedFilterKategori] = useState<number>(1);
  const [prevSelectedFilterKategori, setPrevSelectedFilterKategori] = useState<number>(1); // Menyimpan nilai kategori sebelumnya
  const [isFilterChanged, setIsFilterChanged] = useState<boolean>(false);



  useEffect(() => {
    getListYell();
  }, []);

  useEffect(() => {
    getCauseData()
      .then((data) => setCauseTitles(data))
      .catch((error) => console.error("Error fetching cause titles:", error));
  }, []);

  const getListYell = async () => {
    try {
      const res = await getAllYell();
      const yellArray = Object.values(res) as Yell[];
      const reversedYellArray = yellArray.reverse();
      setYell(reversedYellArray);
    } catch (e: any) {
      console.log(e);
    }
  };

  // Create
  const handleCreateContentChange: React.ChangeEventHandler<
    HTMLInputElement
  > = (e) => {
    setCreateFormValues({
      ...createFormValues,
      content: e.target.value,
    });
  };

  const handleCreateKategoriChange: React.ChangeEventHandler<
    HTMLSelectElement
  > = (e) => {
    setCreateFormValues({
      ...createFormValues,
      kategori: parseInt(e.target.value, 10),
    });
  };

  const handleCreate = async () => {
    if (createFormValues.content == "") {
      toast.error("Yell tidak boleh kosong!");
      console.log("ga boleh kosong");
      getListYell();
      setCreateFormValues({
        content: "",
        kategori: 1, // Set default kategori
      });
    } else {
      try {
        await createPostingan(
          createFormValues.content,
          createFormValues.kategori
        );
        console.log("Yell berhasil dibuat");
      } catch (error) {
        console.log("Gagal membuat Yell:", error);
        toast.success("Yell gagal di post:(");
      } finally {
        getListYell();
        setCreateFormValues({
          content: "",
          kategori: 1, // Set default kategori
        });
        toast.success("Yell berhasil di post!");
      }
    }
  };

  // Vote
  const handleUpvote = async (id: string) => {
    try {
      await votePostingan(id, "upvote");
      getListYell();
      toast.success("Anda berhasil UpVote!");
      console.log(`Upvote berhasil untuk Yell dengan ID ${id}`);
    } catch (error) {
      toast.error("Anda gagal UpVote:(");
      console.log(`Gagal melakukan upvote untuk Yell dengan ID ${id}:`, error);
    }
  };

  const handleDownvote = async (id: string) => {
    try {
      await votePostingan(id, "downvote");
      getListYell();
      toast.success("Anda berhasil DownVote!");
      console.log(`Downvote berhasil untuk Yell dengan ID ${id}`);
    } catch (error) {
      toast.error("Anda gagal DownVote:(");
      console.log(
        `Gagal melakukan downvote untuk Yell dengan ID ${id}:`,
        error
      );
    }
  };

  const handleQuestion = async () => {
    router.push("/postingan/question");
  };

  const [reportForm, setReportForm] = useState({
    cause: "",
    cause_elaboration: "",
    postinganId: "",
  });

  const handleSelectChange: React.ChangeEventHandler<HTMLSelectElement> = (
    e
  ) => {
    setReportForm({
      ...reportForm,
      cause: e.target.value,
    });
  };

  const handleReportChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setReportForm({ ...reportForm, [e.target.name]: e.target.value });
  };

  const handleReport = async () => {
    if (!reportForm.cause || !reportForm.cause_elaboration) {
      toast.error("Please fill in all fields for the report.");
      return;
    }
    try {
      const res = await reportPostingan(
        reportForm.postinganId,
        reportForm.cause,
        reportForm.cause_elaboration
      );
      if (res.error_message === "Postingan ini telah kamu report.") {
        toast.error("You have already reported this post.");
      } else {
        toast.success("Report submitted successfully");
        setReportForm({ cause: "", cause_elaboration: "", postinganId: "" });
        closeReportModal();
      }
    } catch (error) {
      console.error("Error reporting postingan:", error);
      toast.error("Failed to submit report");
    }
  };

  const openReportForm = (postinganId: string) => {
    setCurrentStep(1); // Reset to the first step
    openReportModal(postinganId);
    setReportForm({ ...reportForm, postinganId });
  };

  const openReportModal = (postinganId: string) => {
    setReportForm({ ...reportForm, postinganId });
    setIsReportModalOpen(true);
  };

  const closeReportModal = () => {
    setIsReportModalOpen(false);
    setReportForm({
      cause: "",
      cause_elaboration: "",
      postinganId: "",
    });
  };

  const handleNextStep = () => {
    if (currentStep === 1) {
      if (!reportForm.cause) {
        toast.error("Please select a cause for the report.");
        return;
      }
      setCurrentStep(2);
    } else if (currentStep === 2) {
      handleReport(); 
      closeReportModal(); 
    }
  };

  const handlePreviousStep = () => {
    setCurrentStep(1); 
  };

  const openFilterModal = () => {
    setPrevSelectedFilterKategori(selectedFilterKategori);
    setIsFilterModalOpen(true);
  };

  const closeFilterModal = () => {
    setSelectedFilterKategori(prevSelectedFilterKategori);
    setIsFilterModalOpen(false);
    setIsFilterChanged(false); // Setelah menutup modal, reset status perubahan filter
  };
  const handleFilterKategoriChange: React.ChangeEventHandler<HTMLSelectElement> = (e) => {
    setSelectedFilterKategori(parseInt(e.target.value, 10));
    setIsFilterChanged(true); // Menandai bahwa filter telah diubah
  };

  const applyFilter = async () => {
    try {
      const res = await getYellByKategori(selectedFilterKategori);
      const reversedYellArray = res.reverse();
      setYell(reversedYellArray);
      closeFilterModal(); // Tutup modal setelah menerapkan filter
      setSelectedFilterKategori(selectedFilterKategori);
      setIsFilterChanged(true);
      toast.success("Filter berhasil diterapkan!")
    } catch (error) {
      console.log('Gagal mengambil Yell dengan filter kategori:', error);
    }
  };

  const getFilterLabel = (kategoriId: number) => {
    switch (kategoriId) {
        case 1:
            return 'Akademis';
        case 2:
            return 'Kantin';
        case 3:
            return 'Event';
        case 4:
            return 'Recruitment';
        case 5:
            return 'Fasilitas';
        case 6:
            return 'Out of Topic';
        default:
            return '';
    }
};
  

  return (
    <div className="flex flex-col">
      {/* Navbar */}
      <Navbar />

      <div className="flex">
        {/* Sidebar */}
        <Sidebar isHome={true} />

        {/* Content */}
        <div className="content flex flex-col w-full p-4">
          {/* Container with Buttons */}
          <div className="yq-button flex justify-between items-center bg-white p-4 mb-4">
              {/* Left Section */}
              <div className="flex items-center w-1/2">
                  <button className="border-yell border-b-2 pb-1">
                      Yell
                  </button>
              </div>

              {/* Right Section */}
              <div className="flex items-center w-1/2">
                  <button onClick={handleQuestion} className="border-question border-b-2 pb-1">
                      Question
                  </button>
              </div>
              <div className="flex items-center w-1/6 justify-end">
                  {/* Filter Button */}
                  <button onClick={openFilterModal} className="filter-button bg-blue-500 text-white px-2 py-2 rounded-md ml-5">
                      Filter
                  </button>
              </div>        
              {/* Filter Modal */}
              {isFilterModalOpen && (
                  <div className="modal">
                      <div className="modal-content">
                          <div className="modal-header flex justify-between items-center">
                              <span className="modal-title">Filter Modal</span>
                              <button onClick={closeFilterModal}>
                                  &#10006;
                              </button>
                          </div>
                          <div className="modal-body">
                              <div className="mb-2">
                                  <label htmlFor="filterKategori">Pilih Kategori:</label>
                                  <select
                                      id="filterKategori"
                                      name="filterKategori"
                                      value={selectedFilterKategori}
                                      onChange={handleFilterKategoriChange}
                                      className="kategori-dd p-2 border rounded-md"
                                  >
                                      <option value={1}>Akademis</option>
                                      <option value={2}>Kantin</option>
                                      <option value={3}>Event</option>
                                      <option value={4}>Recruitment</option>
                                      <option value={5}>Fasilitas</option>
                                      <option value={6}>Out of Topic</option>
                                  </select>
                              </div>
                              <button onClick={applyFilter} className="filter-button bg-blue-500 text-white px-2 py-2 rounded-md">
                                  Apply
                              </button>
                          </div>
                      </div>
                  </div>
              )}        
          </div>


            {/* Line */}
            <div className="line" />
            <div className="filtered-properties-section m-5">
              {isFilterChanged ? (
                <><b>Filter sekarang:</b> Kategori - {getFilterLabel(selectedFilterKategori)}</>
              ) : (
                <b>All Yell</b>
              )}
            </div>

          {/* Create Yell */}
          <div className="post-container bg-white p-4">
            {/* Input Text Box */}
            <input
              type="text"
              id="createContent"
              name="content"
              onChange={handleCreateContentChange}
              value={createFormValues.content}
              className="input-box border-none p-2 mb-4"
              placeholder="What’s on your mind?..."
            />

            {/* Grid for Dropdown and Button */}
            <div className="grid grid-cols-2 gap-4">
              {/* Dropdown in First Column */}
              <select
                id="createKategori"
                name="kategori"
                value={createFormValues.kategori}
                onChange={handleCreateKategoriChange}
                className="kategori-dd p-2 border rounded-md mb-2"
              >
                <option value={1}>Akademis</option>
                <option value={2}>Kantin</option>
                <option value={3}>Event</option>
                <option value={4}>Recruitment</option>
                <option value={5}>Fasilitas</option>
                <option value={6}>Out of Topic</option>
              </select>
              {/* Button in Second Column */}
              <button
                onClick={handleCreate}
                className="post-button text-black p-2"
              >
                Post
              </button>
            </div>
          </div>

          {/* Yells */}
          {Yells?.map((yell, i) => (
            <div
              key={yell.id}
              className="yell-container bg-white p-4 mb-4 flex flex-col"
            >
              {/* Row 1: ProfilePhoto, Name, Username, Kategori */}
              <div className="flex items-center mb-2">
                <div className="profile-yell bg-black rounded-full h-7 w-7 mr-2"></div>
                <div className="flex-grow">
                  <p className="font-bold">
                    {yell.mahasiswa.pengguna.nama}
                    <span className="nama-yell text-latto">
                      {" "}
                      - @{yell.mahasiswa.pengguna.npm}{" "}
                    </span>
                  </p>
                </div>
                <div className="text-end">
                  <p className="kategori-yell">{yell.kategori.name}</p>
                </div>
              </div>

              {/* Row 2: Content */}
              <div className="mb-2 pl-9">
                <p>{yell.content}</p>
              </div>

              {/* Row 3: Upvote/Downvote/Message Icons and Reply Button */}
              <div className="flex justify-between items-center pl-9">
                <div className="flex items-center">
                  {/* Upvote */}
                  <button onClick={() => handleUpvote(yell.id)}>
                    <FaThumbsUp className="icon-thumb-up h-4 w-4 mr-2" />
                  </button>
                  <p>{yell.totalUpvote}</p>

                  {/* Downvote */}
                  <button onClick={() => handleDownvote(yell.id)}>
                    <FaThumbsDown className="icon-thumb-down h-4 w-4 ml-4 mr-2" />
                  </button>
                  <p>{yell.totalDownvote}</p>
                </div>
                <div className="space-x-2.5">
                  <Link
                    key={yell.id}
                    href={{
                      pathname: "/postingan/yell/[id]",
                      query: { id: `${yell.id}` },
                    }}
                  >
                    <button className="reply-button bg-orange-color text-black ">
                      Detail
                    </button>
                  </Link>

                  <button
                    onClick={() => openReportForm(yell.id)}
                    className="bg-red-300 px-4 rounded-xl"
                  >
                    Report
                  </button>
                </div>
              </div>
            </div>
          ))}

          {isReportModalOpen && (
            <div className="modal">
              <div className="modal-content space-y-2">
                <h2 className="text-2xl mb-6">Report Yell</h2>
                {currentStep === 1 ? (
                  <div className="flex space-x-4">
                    <label htmlFor="cause" className="text-xl">
                      Cause:
                    </label>
                    <select
                      id="cause"
                      name="cause"
                      value={reportForm.cause}
                      onChange={handleSelectChange}
                      className="text-xl"
                    >
                      <option value="">Select a Cause</option>
                      {causeTitles.map((cause) => (
                        <option key={cause.id} value={cause.id}>
                          {cause.title}
                        </option>
                      ))}
                    </select>
                  </div>
                ) : (
                  <div className="flex flex-col">
                    <label htmlFor="cause_elaboration">Elaboration:</label>
                    <textarea
                      id="cause_elaboration"
                      name="cause_elaboration"
                      value={reportForm.cause_elaboration}
                      onChange={handleReportChange}
                    />
                  </div>
                )}
                <div className="modal-buttons flex  justify-end space-x-5 mr-0 ml-auto">
                  {currentStep === 1 && (
                    <button
                      onClick={closeReportModal}
                      className="bg-red-400 p-3 rounded-xl"
                    >
                      Cancel
                    </button>
                  )}
                  {currentStep === 1 && (
                    <button
                      onClick={handleNextStep}
                      className="bg-yellow-200 p-3 rounded-xl"
                    >
                      Next
                    </button>
                  )}
                  {currentStep === 2 && (
                    <button
                      onClick={handlePreviousStep}
                      className="bg-red-400 p-3 rounded-xl"
                    >
                      Previous
                    </button>
                  )}
                  {currentStep === 2 && (
                    <button
                      onClick={handleNextStep}
                      className="bg-yellow-200 p-3 rounded-xl"
                    >
                      Submit Report
                    </button>
                  )}
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
