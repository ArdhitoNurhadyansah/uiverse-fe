import { useEffect, useState } from 'react';
import { getCurrentUser, updateTentangSaya } from '@/utils/profile';
import UserProfile from '@/models/profile';
import Navbar from '@/components/Navbar';  
import Sidebar from "@/components/Sidebar";
import toast from 'react-hot-toast'

export default function ProfilePage() {
  const [currentUser, setCurrentUser] = useState<UserProfile | null>(null);
  const [tentangSaya, setTentangSaya] = useState<string>("");
  const [tentangSayaBackup, setTentangSayaBackup] = useState<string>("");
  const [isEditingTentangSaya, setIsEditingTentangSaya] = useState<boolean>(false);

  useEffect(() => {
    const fetchCurrentUser = async () => {
      try {
        const user = await getCurrentUser();
        setCurrentUser(user);
        setTentangSaya(user?.profile.tentang_saya || "");
        setTentangSayaBackup(user?.profile.tentang_saya || "");
      } catch (error) {
        console.error('Error fetching current user:', error);
      } finally {
        console.log('Finished fetching current user');
      }
    };

    fetchCurrentUser();
  }, []);

  const handleTentangSayaChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    setTentangSaya(event.target.value);
  };

  const handleEditTentangSaya = () => {
    setIsEditingTentangSaya(true);
  };

  const handleCancelEditTentangSaya = () => {
    // Set kembali nilai tentangSaya ke nilai aslinya
    setTentangSaya(tentangSayaBackup);
    setIsEditingTentangSaya(false);
  };

  const handleSaveTentangSaya = async () => {
    try{
      await updateTentangSaya(tentangSaya);
      toast.success('Tentang saya berhasil diperbarui')
      setTentangSayaBackup(tentangSaya);
    }
    catch(error){
      console.error('Error updating tentang saya:', error);
      toast.error('Gagal memperbarui tentang saya')
      setTentangSaya(tentangSayaBackup);
    }
    finally{
      setIsEditingTentangSaya(false);
    }
  };

  return (
    <div className="flex flex-col">
      {/* Navbar */}
      <Navbar />

      <div className="flex">
        {/* Sidebar */}
        <Sidebar isProfile={true}/>

        {/* Content */}
        <div className="content flex flex-col mt-5 w-full p-4">
          {currentUser && (
            <div className="bg-white p-4 rounded-lg m-10">
              <div className="flex space-x-8">
                {/* User Information */}
                <div className="flex-1">
                  <div>
                    <h2 className="text-xl font-bold mb-2">User Information</h2>
                    <p><span className="font-semibold">Name:</span> {currentUser?.pengguna.nama}</p>
                    <p><span className="font-semibold">Email:</span> {currentUser?.pengguna.email}</p>
                  </div>
                </div>

                {/* Student Profile Information */}
                <div className="flex-1 border-l pl-4 ml-auto">
                  <div className='justify-center items-center'>
                    <h2 className="text-xl font-bold mb-2">Student Profile Information</h2>
                    <p><span className="font-semibold">NPM:</span> {currentUser?.pengguna.npm}</p>
                    <p><span className="font-semibold">Fakultas:</span> {currentUser?.profile.fakultas}</p>
                    <p><span className="font-semibold">Jurusan:</span> {currentUser?.profile.jurusan}</p>
                    <p><span className="font-semibold">Angkatan:</span> {currentUser?.profile.angkatan}</p>
                    <p><span className="font-semibold">Program:</span> {currentUser?.profile.program}</p>
                  </div>
                </div>
              </div>
              {/* Tentang Saya */}
              <div className="mt-4">
                <h2 className="text-xl font-bold mb-2">Tentang Saya</h2>
                {isEditingTentangSaya ? (
                  <>
                    <textarea
                      value={tentangSaya}
                      onChange={handleTentangSayaChange}
                      rows={4}
                      className="w-full border rounded-md p-2"
                      placeholder="Tulis sesuatu tentang diri Anda..."
                    />
                    <div className="mt-2 flex space-x-2">
                      <button onClick={handleSaveTentangSaya} className="bg-yellow-300 text-white px-4 py-2 rounded-md">
                        Simpan Perubahan
                      </button>
                      <button onClick={handleCancelEditTentangSaya} className="bg-gray-300 text-black px-4 py-2 rounded-md">
                        Batal
                      </button>
                    </div>
                  </>
                ) : (
                  <>
                    <p>{tentangSaya}</p>
                    <button onClick={handleEditTentangSaya} className="mt-2 bg-yellow-300 text-white px-4 py-2 rounded-md">
                      Edit
                    </button>
                  </>
                )}
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
