import React from "react";
import { useEffect, useState } from "react";
import {
  getCauseData,
  getKategoriPostinganData,
  getAllReportedPostingan,
  getSpecificReportedPostingan,
  takeDownPostingan,
} from "@/utils/report";

import Link from "next/link";
import toast from "react-hot-toast";
interface Pengguna {
  id: string;
  npm: string;
  nama: string;
  email: string;
}

interface Profile {
  fakultas: string;
  jurusan: string;
  angkatan: string;
  program: string;
  tentang_saya: string;
  profile_picture: string | null;
}

interface Mahasiswa {
  role: string;
  pengguna: Pengguna;
  totalFollowers: number;
  totalUpvote: number;
  totalDownvote: number;
  profile: Profile;
}

interface Kategori {
  id: number;
  name: string;
}

interface Postingan {
  id: string;
  mahasiswa: Mahasiswa;
  kategori: Kategori;
  content: string;
  totalUpvote: number;
  totalDownvote: number;
  createdAt: string;
  is_taken_down: boolean;
}

interface ReportedPostingan {
  id: string;
  cause_title: string;
  postingan: Postingan;
  timestamp: string;
  cause_elaboration: string;
  reporter: string;
}

type SortField = "totalDownvote" | "totalUpvote" | "timestamp";

export default function ReportsPage() {
  const [causeTitles, setCauseTitles] = useState<
    { id: string; title: string }[]
  >([]);
  const [kategoriPostingan, setKategoriPostingan] = useState<
    { id: string; name: string }[]
  >([]);

  const [reports, setReports] = useState<ReportedPostingan[]>([]);
  const [isLoading, setLoading] = useState(true);

  const [filterCauseTitle, setFilterCauseTitle] = useState<string>("");
  const [filterKategori, setFilterKategori] = useState<string>("");
  const [filterIsTakenDown, setFilterIsTakenDown] = useState<boolean | null>(
    null
  );

  const [sortField, setSortField] = useState<SortField>("timestamp");
  const [sortOrder, setSortOrder] = useState<string>("ascending");

  const [isDetailModalOpen, setIsDetailModalOpen] = useState(false);
  const [selectedReport, setSelectedReport] =
    useState<ReportedPostingan | null>(null);

  const [isFetchingDetails, setIsFetchingDetails] = useState(false);

  useEffect(() => {
    getAllReportedPostingan()
      .then((data: ReportedPostingan[]) => {
        setReports(data);
        setLoading(false);
      })
      .catch((error) => {
        console.error("Error fetching reports:", error);
        setLoading(false);
      });
  }, []);

  useEffect(() => {
    getCauseData()
      .then((data) => setCauseTitles(data))
      .catch((error) => console.error("Error fetching cause titles:", error));

    getKategoriPostinganData()
      .then((data) => setKategoriPostingan(data))
      .catch((error) =>
        console.error("Error fetching kategori postingan:", error)
      );
  }, []);

  const filteredAndSortedReports = reports
    .filter(
      (report) =>
        (filterCauseTitle === "" || report.cause_title === filterCauseTitle) &&
        (filterKategori === "" ||
          report.postingan.kategori.name === filterKategori) &&
        (filterIsTakenDown === null ||
          report.postingan.is_taken_down === filterIsTakenDown)
    )
    .sort((a, b) => {
      let fieldA: number;
      let fieldB: number;

      if (sortField === "timestamp") {
        fieldA = new Date(a.timestamp).getTime();
        fieldB = new Date(b.timestamp).getTime();
      } else {
        const aValue = a.postingan[sortField as keyof typeof a.postingan];
        const bValue = b.postingan[sortField as keyof typeof b.postingan];

        fieldA = typeof aValue === "number" ? aValue : 0;
        fieldB = typeof bValue === "number" ? bValue : 0;
      }

      return sortOrder === "ascending" ? fieldA - fieldB : fieldB - fieldA;
    });

  const fetchReportDetails = async (postinganId: string) => {
    setIsFetchingDetails(true);
    try {
      const reportDetails = await getSpecificReportedPostingan(postinganId);
      setSelectedReport(reportDetails);
      setIsDetailModalOpen(true);
    } catch (error) {
      console.error("Error fetching report details:", error);
    }
    setIsFetchingDetails(false);
  };

  const confirmTakeDown = async (postinganId: string) => {
    const confirm = window.confirm(
      "Are you sure you want to take down this post?"
    );
    if (confirm) {
      try {
        const res = await takeDownPostingan(postinganId);
        if (res.error_message === "Postingan ini telah ditakedown.") {
          toast.error("You have already reported this post.");
        }
      } catch (error) {
        console.error("Error taking down the post:", error);
      }
    }
  };

  if (isLoading) return <p>Loading...</p>;

  return (
    <div className="container mx-auto p-4">
      <div className="flex space-x-10 justify-center items-center">
        <ul className="flex space-x-10">
          <li>
            <Link href={"/reported/postingan"}>Reported Postingan</Link>
          </li>

          <li>
            <Link href={"/reported/tanggapan"}>Reported Tanggapan</Link>
          </li>

          <li>
            <Link href={"/taken-down"}>Taken Down</Link>
          </li>
        </ul>
      </div>
      <div className="p-3 m-5 text-center items-center justify-center">
        <h1 className="font-sans text-3xl">Reported Postingan Page</h1>
      </div>
      <div className="flex space-x-4 mb-4">
        <div>
          <label htmlFor="causeTitleFilter">Cause:</label>
          <select
            id="causeTitleFilter"
            value={filterCauseTitle}
            onChange={(e) => setFilterCauseTitle(e.target.value)}
          >
            <option value="">All Causes</option>
            {causeTitles.map((cause) => (
              <option key={cause.id} value={cause.title}>
                {cause.title}
              </option>
            ))}
          </select>
        </div>

        <div>
          <label htmlFor="kategoriFilter">Kategori:</label>
          <select
            id="kategoriFilter"
            value={filterKategori}
            onChange={(e) => setFilterKategori(e.target.value)}
          >
            <option value="">All Categories</option>
            {kategoriPostingan.map((kategori) => (
              <option key={kategori.id} value={kategori.name}>
                {kategori.name}
              </option>
            ))}
          </select>
        </div>

        <div>
          <label htmlFor="takenDownFilter">Taken Down:</label>
          <select
            id="takenDownFilter"
            value={
              filterIsTakenDown !== null ? filterIsTakenDown.toString() : ""
            }
            onChange={(e) => {
              const value = e.target.value;
              if (value === "true") {
                setFilterIsTakenDown(true);
              } else if (value === "false") {
                setFilterIsTakenDown(false);
              } else {
                setFilterIsTakenDown(null);
              }
            }}
          >
            <option value="">All</option>
            <option value="true">Taken Down</option>
            <option value="false">Active</option>
          </select>
        </div>

        <div>
          <label htmlFor="sortField">Sort by:</label>
          <select
            id="sortField"
            value={sortField}
            onChange={(e) => setSortField(e.target.value as SortField)}
          >
            <option value="totalDownvote">Total Downvotes</option>
            <option value="totalUpvote">Total Upvotes</option>
            <option value="timestamp">Timestamp</option>
          </select>
        </div>

        <div>
          <button
            onClick={() =>
              setSortOrder(
                sortOrder === "ascending" ? "descending" : "ascending"
              )
            }
            className="bg-white px-4"
          >
            {sortOrder === "ascending" ? "Sort Descending" : "Sort Ascending"}
          </button>
        </div>
      </div>

      {filteredAndSortedReports.map((report) => (
        <div
          key={report.id}
          className="bg-white shadow-md rounded p-4 mb-4 flex space-x-5"
        >
          <div className="flex-grow">
            <h2 className="text-2xl font-semibold ">
              Main Cause: {report.cause_title}
            </h2>
            <p className="text-xl font-semibold">
              Elaboration: {report.cause_elaboration}
            </p>
            <div className="pt-2">
              <p className="font-medium">Post Summary:</p>
              <p>Content: {report.postingan.content}</p>
              <p>Category: {report.postingan.kategori.name}</p>
              <p>
                Created at:{" "}
                {new Date(report.postingan.createdAt).toLocaleString()}
              </p>
              <p>
                Upvotes: {report.postingan.totalUpvote} | Downvotes:{" "}
                {report.postingan.totalDownvote}
              </p>
              <p>
                Status:{" "}
                {report.postingan.is_taken_down ? "Taken Down" : "Active"}
              </p>
            </div>
            <div className="pt-2">
              <p className="font-medium">Reported by:</p>
              <p>{report.reporter}</p>
            </div>
          </div>

          <div className="mr-0 ml-auto ">
            <button
              onClick={() => fetchReportDetails(report.id)}
              disabled={isFetchingDetails}
              className="mr-2 p-4 bg-yellow-50 rounded-xl"
            >
              See Detail
            </button>
            <button
              onClick={() => confirmTakeDown(report.postingan.id)}
              className="bg-red-400 p-4 mr-2 rounded-xl"
            >
              Take Down
            </button>
          </div>
        </div>
      ))}

      {isDetailModalOpen && selectedReport && (
        <div className="modal">
          <div className="modal-content">
            <div className="modal-header">
              Main Cause: {selectedReport.cause_title}
            </div>
            <div className="modal-body">
              <p>
                <strong>ID:</strong> {selectedReport.id}
              </p>
              <p>
                <strong>Cause Title:</strong> {selectedReport.cause_title}
              </p>
              <p>
                <strong>Post Summary:</strong>{" "}
                {selectedReport.postingan.content}
              </p>
              <p>
                <strong>Category:</strong>{" "}
                {selectedReport.postingan.kategori.name}
              </p>
              <p>
                <strong>Reported By:</strong> {selectedReport.reporter}
              </p>
              <p>
                <strong>Timestamp:</strong>{" "}
                {new Date(selectedReport.timestamp).toLocaleString()}
              </p>

              <br />

              <p className="text-xl font-bold">Content by:</p>
              <p>
                <strong>Role:</strong> {selectedReport.postingan.mahasiswa.role}
              </p>
              <p>
                <strong>Name:</strong>{" "}
                {selectedReport.postingan.mahasiswa.pengguna.nama}
              </p>
              <p>
                <strong>Email:</strong>{" "}
                {selectedReport.postingan.mahasiswa.pengguna.email}
              </p>
              <p>
                <strong>Total Followers:</strong>{" "}
                {selectedReport.postingan.mahasiswa.totalFollowers}
              </p>
              <p>
                <strong>Upvotes:</strong> {selectedReport.postingan.totalUpvote}
              </p>
              <p>
                <strong>Downvotes:</strong>{" "}
                {selectedReport.postingan.totalDownvote}
              </p>
              <p>
                <strong>Faculty:</strong>{" "}
                {selectedReport.postingan.mahasiswa.profile.fakultas}
              </p>
              <p>
                <strong>Major:</strong>{" "}
                {selectedReport.postingan.mahasiswa.profile.jurusan}
              </p>
            </div>
            <button
              onClick={() => setIsDetailModalOpen(false)}
              className="close-button"
            >
              Close
            </button>
          </div>
        </div>
      )}
    </div>
  );
}
