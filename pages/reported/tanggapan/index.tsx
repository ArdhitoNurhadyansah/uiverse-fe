import React, { useEffect, useState } from "react";
import {
  getCauseData,
  getSpecificReportedTanggapan,
  takeDownTanggapan,
  getAllReportedTanggapan,
} from "@/utils/report";
import Link from "next/link";
import toast from "react-hot-toast";
interface Pengguna {
  id: string;
  npm: string;
  nama: string;
  email: string;
}

interface Profile {
  fakultas: string;
  jurusan: string;
  angkatan: string;
  program: string;
  tentang_saya: string;
  profile_picture: string | null;
}

interface Mahasiswa {
  role: string;
  pengguna: Pengguna;
  totalFollowers: number;
  totalUpvote: number;
  totalDownvote: number;
  profile: Profile;
}

interface Kategori {
  id: number;
  name: string;
}

interface Postingan {
  id: string;
  mahasiswa: Mahasiswa;
  kategori: Kategori;
  content: string;
  totalUpvote: number;
  totalDownvote: number;
  createdAt: string;
  is_taken_down: boolean;
}

interface Tanggapan {
  id: string;
  mahasiswa: Mahasiswa;
  postingan: Postingan;
  content: string;
  totalUpvote: number;
  totalDownvote: number;
  createdAt: string;
  is_taken_down: boolean;
}

interface ReportedTanggapan {
  id: string;
  cause_title: string;
  tanggapan: Tanggapan;
  timestamp: string;
  cause_elaboration: string;
  reporter: string;
}

export default function ReportedTanggapanPage() {
  const [causeTitles, setCauseTitles] = useState<
    { id: string; title: string }[]
  >([]);
  const [reportedTanggapan, setReportedTanggapan] = useState<
    ReportedTanggapan[]
  >([]);
  const [isLoading, setLoading] = useState(true);
  const [selectedTanggapan, setSelectedTanggapan] =
    useState<ReportedTanggapan | null>(null);
  const [isDetailModalOpen, setIsDetailModalOpen] = useState(false);

  useEffect(() => {
    getAllReportedTanggapan()
      .then((data: ReportedTanggapan[]) => {
        setReportedTanggapan(data);
        setLoading(false);
      })
      .catch((error) => {
        console.error("Error fetching reported tanggapan:", error);
        setLoading(false);
      });

    getCauseData()
      .then((data) => setCauseTitles(data))
      .catch((error) => console.error("Error fetching cause titles:", error));
  }, []);

  const fetchTanggapanDetails = async (tanggapanId: string) => {
    try {
      const tanggapanDetails = await getSpecificReportedTanggapan(tanggapanId);
      setSelectedTanggapan(tanggapanDetails);
      setIsDetailModalOpen(true);
    } catch (error) {
      console.error("Error fetching tanggapan details:", error);
    }
  };

  const confirmTakeDown = async (tanggapanId: string) => {
    if (window.confirm("Are you sure you want to take down this response?")) {
      try {
        const res = await takeDownTanggapan(tanggapanId);
        if (res.error_message === "Tanggapan ini telah ditakedown.") {
            toast.error("You have already reported this post.");
          }
      } catch (error) {
        console.error("Error taking down the response:", error);
      }
    }
  };

  if (isLoading) return <p>Loading...</p>;

  return (
    <div className="container mx-auto p-4">
      <div className="flex space-x-10  justify-center items-center mt-4 mb-7">
        <ul className="flex space-x-10">
          <li>
            <Link href={"/reported/postingan"}>Reported Postingan</Link>
          </li>

          <li>
            <Link href={"/reported/tanggapan"}>Reported Tanggapan</Link>
          </li>

          <li>
            <Link href={"/taken-down"}>Taken Down</Link>
          </li>
        </ul>
      </div>

      <p className="text-3xl text-center mb-6">Reported Tanggapan</p>

      {reportedTanggapan.map((report) => (
        <div
          key={report.id}
          className="bg-white shadow-md rounded p-4 mb-4 flex space-x-5"
        >
          <div className="flex-grow">
            <h2 className="text-2xl font-semibold">
              Main Cause: {report.cause_title}
            </h2>
            <p className="text-xl font-semibold">
              Elaboration: {report.cause_elaboration}
            </p>
            <div className="pt-2">
              <p className="font-semibold">
                Content: {report.tanggapan.content}
              </p>
              <p>
                <strong>Reported By:</strong> {report.reporter}
              </p>
              <p>
                <strong>Timestamp:</strong>{" "}
                {new Date(report.timestamp).toLocaleString()}
              </p>
              <p>
                <strong>Is Taken Down:</strong>{" "}
                {report.tanggapan.is_taken_down.toString()}
              </p>
            </div>
          </div>
          <div className="mr-0 ml-auto ">
            <button
              onClick={() => fetchTanggapanDetails(report.id)}
              className="mr-2 p-4 bg-yellow-50 rounded-xl"
            >
              See Detail
            </button>
            <button
              onClick={() => confirmTakeDown(report.tanggapan.id)}
              className="bg-red-400 p-4 mr-2 rounded-xl"
            >
              Take Down
            </button>
          </div>
        </div>
      ))}

      {isDetailModalOpen && selectedTanggapan && (
        <div className="modal">
          <div className="modal-content">
            <div className="modal-header">
              Main Cause: {selectedTanggapan.cause_title}
            </div>
            <div className="modal-body">
              <p>
                <strong>ID:</strong> {selectedTanggapan.id}
              </p>
              <p>
                <strong>Cause Title:</strong> {selectedTanggapan.cause_title}
              </p>
              <p>
                <strong>Content:</strong> {selectedTanggapan.tanggapan.content}
              </p>
              <p>
                <strong>Category:</strong>{" "}
                {selectedTanggapan.tanggapan.postingan.kategori.name}
              </p>
              <p>
                <strong>Reported By:</strong> {selectedTanggapan.reporter}
              </p>
              <p>
                <strong>Timestamp:</strong>{" "}
                {new Date(selectedTanggapan.timestamp).toLocaleString()}
              </p>

              <p>
                <strong>Is Taken Down:</strong>{" "}
                {selectedTanggapan.tanggapan.is_taken_down.toString()}
              </p>
            </div>
            <button
              onClick={() => setIsDetailModalOpen(false)}
              className="modal-close"
            >
              Close
            </button>
          </div>
        </div>
      )}
    </div>
  );
}
