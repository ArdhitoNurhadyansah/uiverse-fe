import React, { useEffect, useState } from "react";
import {
  getAllTakenDownPostingan,
  restoreTakenDownPostingan,
  getAllTakenDownTanggapan,
  restoreTakenDownTanggapan,
} from "@/utils/report";

import Link from "next/link";

interface Pengguna {
  id: string;
  npm: string;
  nama: string;
  email: string;
}

interface Profile {
  fakultas: string;
  jurusan: string;
  angkatan: string;
  program: string;
  tentang_saya: string;
  profile_picture: string | null;
}

interface Mahasiswa {
  role: string;
  pengguna: Pengguna;
  totalFollowers: number;
  totalUpvote: number;
  totalDownvote: number;
  profile: Profile;
}

interface Kategori {
  id: number;
  name: string;
}

interface Postingan {
  id: string;
  mahasiswa: Mahasiswa;
  kategori: Kategori;
  content: string;
  totalUpvote: number;
  totalDownvote: number;
  createdAt: string;
  is_taken_down: boolean;
}

interface TakenDownPostingan {
  id: number;
  postingan: Postingan;
  admin: string;
  timestamp: string;
}

interface Tanggapan {
  id: string;
  mahasiswa: Mahasiswa;
  postingan: Postingan;
  content: string;
  timestamp: string;
  is_taken_down: boolean;
}

interface TakenDownContent {
  id: number;
  tanggapan?: Tanggapan;
  postingan?: Postingan;
  admin: string;
  timestamp: string;
}

type ContentType = "postingan" | "tanggapan";

export default function TakenDownPage() {
  const [contentType, setContentType] = useState<ContentType>("postingan");
  const [takenDownContent, setTakenDownContent] = useState<TakenDownContent[]>(
    []
  );
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    const fetchContent =
      contentType === "postingan"
        ? getAllTakenDownPostingan
        : getAllTakenDownTanggapan;
    fetchContent()
      .then((data: TakenDownPostingan[]) => {
        setTakenDownContent(data);
        setLoading(false);
      })
      .catch((error) => {
        console.error("Error fetching taken-down posts:", error);
        setLoading(false);
      });
  }, [contentType]);

  const handleRestore = async (contentId: string) => {
    const confirm = window.confirm(
      `Are you sure you want to restore this ${contentType}?`
    );
    if (confirm) {
      try {
        const restoreFunction =
          contentType === "postingan"
            ? restoreTakenDownPostingan
            : restoreTakenDownTanggapan;
        await restoreFunction(contentId);
      } catch (error) {
        console.error(`Error restoring the ${contentType}:`, error);
      }
    }
  };

  if (isLoading)
    return (
      <div className="items-center justify-center">
        <div className="flex space-x-10  justify-center items-center">
          <ul className="flex space-x-10">
            <li>
              <Link href={"/reported/postingan"}>Reported Postingan</Link>
            </li>

            <li>
              <Link href={"/reported/tanggapan"}>Reported Tanggapan</Link>
            </li>

            <li>
              <Link href={"/taken-down"}>Taken Down</Link>
            </li>
          </ul>
        </div>
        <p>Loading...</p>
      </div>
    );

  

  return (
    <div className="container mx-auto p-4">
      <div className="flex space-x-10  justify-center items-center">
        <ul className="flex space-x-10">
          <li>
            <Link href={"/reported/postingan"}>Reported Postingan</Link>
          </li>

          <li>
            <Link href={"/reported/tanggapan"}>Reported Tanggapan</Link>
          </li>

          <li>
            <Link href={"/taken-down"}>Taken Down</Link>
          </li>
        </ul>
      </div>

      <div className="mb-4">
        <select
          value={contentType}
          onChange={(e) => setContentType(e.target.value as ContentType)}
        >
          <option value="postingan">Postingan</option>
          <option value="tanggapan">Tanggapan</option>
        </select>
      </div>

      {takenDownContent.map((content) => (
        <div key={content.id} className="bg-white shadow-md rounded p-4 mb-4">
          {contentType === "postingan" && content.postingan && (
            <>
              <div>
                <h2 className="text-xl font-semibold">
                  Post created by: {content.postingan.mahasiswa.pengguna.nama}
                </h2>
                <p className="text-gray-600">
                  Content: {content.postingan.content}
                </p>
              </div>

              <div>
                <h2>TakenDown By: {content.admin}</h2>
                <h2>TakenDown At: {content.timestamp}</h2>
              </div>
            </>
          )}
          {contentType === "tanggapan" && content.tanggapan && (
            <>
              <h2 className="text-xl font-semibold">
                By: {content.tanggapan.mahasiswa.pengguna.nama}
              </h2>
              <p className="text-gray-600">
                Content: {content.tanggapan.content}
              </p>

              <div>
                <h2>TakenDown By: {content.admin}</h2>
                <h2>TakenDown At: {content.timestamp}</h2>
              </div>
            </>
          )}
          <button
            className="p-3 bg-yellow-50 rounded-xl"
            onClick={() => {
              const contentId =
                contentType === "postingan"
                  ? content.postingan?.id
                  : content.tanggapan?.id;
              if (contentId) {
                handleRestore(contentId);
              }
            }}
          >
            Restore {contentType.charAt(0).toUpperCase() + contentType.slice(1)}
          </button>
        </div>
      ))}
    </div>
  );
}
