// const BASE_API_URL = "http://localhost:8000/api";
const BASE_API_URL = "https://uiverse-be.vercel.app/api";


export const loginMahasiswa = async (username: string, password: string) => {
  const response = await fetch(`${BASE_API_URL}/auth/login/mahasiswa/`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ username, password }),
    credentials: "include",
  });

  if (response.status !== 200) {
    const data = await response.json();
    throw new Error(data);
  }

  return response.json();
};

export const loginAdmin = async (username: string, password: string) => {
  const response = await fetch(`${BASE_API_URL}/auth/login/admin/`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ username, password }),
    credentials: "include",
  });

  if (response.status !== 200) {
    const data = await response.json();
    throw new Error(data);
  }
  return response.json();
};

export const logout = async () => {
  try {
    const response = await fetch(`${BASE_API_URL}/auth/logout/`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "Authorization": `Bearer ${localStorage.getItem('token')}`,
      },
      credentials: "include",
    });

    if (!response.ok) {
      const data = await response.json();
      throw new Error(data);
    }

    return response.json();
  } catch (error) {
    console.error('Logout failed:', error);
    throw error; // Rethrow the error to propagate it to the caller
  }
};