// UserContext.tsx
import { createContext, ReactNode, useContext, useState, useEffect } from 'react';
import { getCurrentUser } from '@/utils/profile'; // Sesuaikan path sesuai struktur proyek Anda

interface UserContextProps {
    children: ReactNode;
  }
  
  interface UserData {
    username: string;
    fullName: string;
    email: string;
    profilePicture: string | null; // Tambahkan field profilePicture
  }
  
  interface UserContextType {
    user: UserData | null;
    setUser: (userData: UserData | null) => void;
  }
  
  const UserContext = createContext<UserContextType | undefined>(undefined);
  
  export const UserProvider: React.FC<UserContextProps> = ({ children }) => {
    const [user, setUser] = useState<UserData | null>(null);
  
    useEffect(() => {
      const fetchUser = async () => {
        try {
          const userData = await getCurrentUser();
          const simplifiedUserData: UserData = {
            username: userData.pengguna.id,
            fullName: userData.pengguna.nama,
            email: userData.pengguna.email,
            profilePicture: userData.profile.profile_picture,
          };
          setUser(simplifiedUserData);
        } catch (error) {
          console.error('Error fetching user data:', error);
        }
      };
  
      fetchUser();
    }, []); // useEffect dependency array is empty, so it runs once on component mount
  
    return (
      <UserContext.Provider value={{ user, setUser }}>
        {children}
      </UserContext.Provider>
    );
  };
  
  export const useUser = () => {
    const context = useContext(UserContext);
    if (!context) {
      throw new Error('useUser must be used within a UserProvider');
    }
    return context;
  };