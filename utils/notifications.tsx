const BASE_API_URL = "http://localhost:8000/api";

export const pushNotifications = async (finalMessage: string, reciever: string, sender: string, event:string) => {
    const token = localStorage.getItem('token');
    const role = localStorage.getItem("role") || '';
  
    const response = await fetch(`${BASE_API_URL}/notifications/create/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
        'role': role,
      },
      credentials: "include",
      body: JSON.stringify({
        finalMessage: finalMessage,
        reciever: reciever,
        sender: sender,
        event: event,
      }),
    });
  
    if (response.status !== 200) {
      const data = await response.json();
      throw new Error(data);
    }
  
    return response.json();
};

export const getAllNotifications = async () => {
    const token = localStorage.getItem('token');
    const role = localStorage.getItem("role") || '';
  
    const response = await fetch(`${BASE_API_URL}/notifications/all/`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
        'role': role,
      },
      credentials: "include",
    });
  
    if (response.status !== 200) {
      const data = await response.json();
      throw new Error(data);
    }
  
    return response.json();
};