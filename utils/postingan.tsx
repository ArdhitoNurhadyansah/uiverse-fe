// const BASE_API_URL = "https://uiverse-be.vercel.app/api";
// const BASE_API_URL = "http://localhost:8000/api";
const BASE_API_URL = "https://uiverse-be.vercel.app/api"

// Postingan

export const deletePostingan = async (id: string) => {
    const token = localStorage.getItem('token');
    const role = localStorage.getItem("role") || '';
  
    const response = await fetch(`${BASE_API_URL}/postingan/delete/${id}/`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
        'role': role,
      },
      credentials: "include",
    });
  
    if (response.status !== 200) {
      const data = await response.json();
      throw new Error(data);
    }
  
    return response.json();
};

export const updatePostingan = async (id: string, newContent: string, newKategori: number) => {
    const token = localStorage.getItem('token');
    const role = localStorage.getItem("role") || '';
  
    const response = await fetch(`${BASE_API_URL}/postingan/update/${id}/`, {
      method: 'PUT', 
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
        'role': role,
      },
      credentials: "include",
      body: JSON.stringify({
        content: newContent,
        kategori: newKategori,
      }),
    });
  
    if (response.status !== 200) {
      const data = await response.json();
      console.error(data); // Log the error details
      throw new Error(data);
    }
  
    return response.json();
};

export const createPostingan = async (content: string, kategori: number) => {
    const token = localStorage.getItem('token');
    const role = localStorage.getItem("role") || '';
  
    const response = await fetch(`${BASE_API_URL}/postingan/create/`, {
      method: 'POST', 
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
        'role': role,
      },
      credentials: "include",
      body: JSON.stringify({
        content: content,
        kategori: kategori,
      }),
    });
  
    if (response.status !== 200) {
      const data = await response.json();
      console.error(data); // Log the error details
      throw new Error(data);
    }
  
    return response.json();
};

export const votePostingan = async (id: string, vote:string) => {
    const token = localStorage.getItem('token');
    const role = localStorage.getItem("role") || '';
  
    const response = await fetch(`${BASE_API_URL}/postingan/vote/${id}/`, {
      method: 'POST', 
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
        'role': role,
      },
      credentials: "include",
      body: JSON.stringify({
        vote,
      }),
    });
  
    if (response.status !== 200) {
      const data = await response.json();
      throw new Error(data);
    }
  
    return response.json();
};

// Yell

export const getAllYell = async () => {
    const token = localStorage.getItem('token');
    const role = localStorage.getItem("role") || '';
    console.log(role)
     
    const response = await fetch(`${BASE_API_URL}/postingan/yell/`, {
      headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
          'role': role,
        },
      credentials: "include",
    });
  
    if (response.status !== 200) {
      const data = await response.json();
      throw new Error(data);
    }
  
    return response.json();
};
  
export const getYell = async (id: string) => {
    const token = localStorage.getItem('token');
    const role = localStorage.getItem("role") || '';
       
    const response = await fetch(`${BASE_API_URL}/postingan/${id}/`, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`,
            'role': role,
          },
        credentials: "include",
    });
    
    if (response.status !== 200) {
        const data = await response.json();
        throw new Error(data);
    }
    
    return response.json();
}

export const getVote = async (postinganId: String) => {
  const token = localStorage.getItem('token');
  const role = localStorage.getItem("role") || '';
     
  const response = await fetch(`${BASE_API_URL}/postingan/getvote/${postinganId}/`, {
      headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${token}`,
          'role': role,
        },
      credentials: "include",
  });
  
  if (response.status !== 200) {
    const data = await response.json();
    throw new Error(data);
  }
  
  return response.json();
}

// Question
export const createQuestion = async (content: string, kategori: number) => {
  const token = localStorage.getItem('token');
  const role = localStorage.getItem("role") || '';

  const response = await fetch(`${BASE_API_URL}/postingan/question/`, {
    method: 'POST', 
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`,
      'role': role,
    },
    credentials: "include",
    body: JSON.stringify({
      content: content,
      kategori: kategori,
    }),
  });

  if (response.status !== 200) {
    const data = await response.json();
    console.error(data); // Log the error details
    throw new Error(data);
  }

  return response.json();
};

export const getAllQuestion = async () => {
  const token = localStorage.getItem('token');
  const role = localStorage.getItem("role") || '';
  console.log(role)
   
  const response = await fetch(`${BASE_API_URL}/postingan/question/`, {
    method: 'GET', 
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
        'role': role,
      },
    credentials: "include",
  });

  if (response.status !== 200) {
    const data = await response.json();
    throw new Error(data);
  }

  return response.json();
};

export const getQuestion = async (id: string) => {
  const token = localStorage.getItem('token');
  const role = localStorage.getItem("role") || '';
     
  const response = await fetch(`${BASE_API_URL}/postingan/question/${id}/`, {
    method: "GET",
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
        'role': role,
      },
    credentials: "include",
  });
  
  if (response.status !== 200) {
      const data = await response.json();
      throw new Error(data);
  }
  
  return response.json();
}


export const followQuestion = async (id: string) => {
  const token = localStorage.getItem('token');
  const role = localStorage.getItem("role") || '';

  const response = await fetch(`${BASE_API_URL}/postingan/question/follow/${id}/`, {
    method: 'POST', 
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`,
      'role': role,
    },
    credentials: "include",
  });

  if (response.status !== 200) {
    const data = await response.json();
    console.error(data); // Log the error details
    throw new Error(data);
  }

  return response.json();
};

// Reply
export const createReply = async (content: string, id:any) => {
  const token = localStorage.getItem('token');
  const role = localStorage.getItem('role') || '';

  const response = await fetch(`${BASE_API_URL}/postingan/reply/create/${id}/`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`,
      'role': role,
    },
    credentials: "include",
    body: JSON.stringify({
      content: content
    }),
  });
  if (response.status != 200){
    const data = await response.json();
    console.error(data)
    throw new Error(data)
  }
  return response.json()
}

// Answer
export const createAnswer = async (content: string, id:string) => {
  const token = localStorage.getItem('token');
  const role = localStorage.getItem('role') || '';

  const response = await fetch(`${BASE_API_URL}/postingan/answer/create/${id}/`,{
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`,
      'role': role,
    },
    credentials: 'include',
    body: JSON.stringify({
      content: content
    }),
  });
  if (response.status != 200){
    const data = await response.json()
    console.error(data)
    throw new Error(data)
  }
  return response.json()
}

// Answer
export const getAllAnswers = async (id: string) => {
  const token = localStorage.getItem('token');
  const role = localStorage.getItem("role") || '';
  console.log(role)
   
  const response = await fetch(`${BASE_API_URL}/postingan/answer/all/${id}/`, {
    method: 'GET', 
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
        'role': role,
      },
    credentials: "include",
  });

  if (response.status !== 200) {
    const data = await response.json();
    throw new Error(data);
  }

  return response.json();
};

export const getYellByKategori = async (kategoriId: number) => {
  const token = localStorage.getItem('token');
  const role = localStorage.getItem("role") || '';
  console.log(role)
   
  const response = await fetch(`${BASE_API_URL}/postingan/yell/kategori/${kategoriId}/`, {
    method: 'GET', 
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
        'role': role,
      },
    credentials: "include",
  });

  if (response.status !== 200) {
    const data = await response.json();
    throw new Error(data);
  }

  return response.json();
};

export const getQuestionByKategori = async (kategoriId: number) => {
  const token = localStorage.getItem('token');
  const role = localStorage.getItem("role") || '';
  console.log(role)
   
  const response = await fetch(`${BASE_API_URL}/postingan/question/kategori/${kategoriId}/`, {
    method: 'GET', 
    headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`,
        'role': role,
      },
    credentials: "include",
  });

  if (response.status !== 200) {
    const data = await response.json();
    throw new Error(data);
  }

  return response.json();
};