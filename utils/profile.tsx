// const BASE_API_URL = "https://uiverse-be.vercel.app/api";
const BASE_API_URL = "https://uiverse-be.vercel.app/api";

// const BASE_API_URL = "https://uiverse-be.vercel.app/api"

export const getCurrentUser = async () => {
    const token = localStorage.getItem('token');
    const role = localStorage.getItem("role") || '';
    const response = await fetch(`${BASE_API_URL}/profile/`, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          'Authorization': `Bearer ${token}`,
          'role': role,
        },
        credentials: "include",
      });
    
      if (response.status !== 200) {
        const data = await response.json();
        throw new Error(data);
      }
    
      return response.json();
}

export const updateTentangSaya = async (tentang_saya: string) => {
  const token = localStorage.getItem('token');
  const role = localStorage.getItem("role") || '';
  const response = await fetch(`${BASE_API_URL}/profile/update/`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        'Authorization': `Bearer ${token}`,
        'role': role,
      },
      credentials: "include",
      body: JSON.stringify({
        tentang_saya: tentang_saya,
      }),
    });
  
    if (response.status !== 200) {
      const data = await response.json();
      throw new Error(data);
    }
  
    return response.json();
}
