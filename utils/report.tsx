// const BASE_API_URL = "http://localhost:8000/api";
const BASE_API_URL = "https://uiverse-be.vercel.app/api"

import supabase from './supabase';

export const getCauseData = async () => {
  const { data, error } = await supabase
    .from('admin_report_reportcause')
    .select('id, title')
    .order('id', {ascending: true});

  if (error) throw error;
  return data;
};


export const  getKategoriPostinganData = async () => {
  const { data, error } = await supabase
    .from('postingan_kategoripostingan')
    .select('id, name')
    .order('id', {ascending: true});

  if (error) throw error;
  return data;
};

export const reportPostingan = async (
  postinganId: string,
  cause: string,
  cause_elaboration: string
) => {
  const token = localStorage.getItem("token");
  const role = localStorage.getItem("role") || "";
  const response = await fetch(
    `${BASE_API_URL}/report/report-postingan/${postinganId}/`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
        role: role,
      },
      body: JSON.stringify({ cause, cause_elaboration }),
      credentials: "include",
    }
  );


  if (response.status !== 201) {
    const data = await response.json();
    if(data.error_message){
      return data;
    }
    throw new Error(data);
  }

  return response.json();
};

export const reportTanggapan = async (
  tanggapanId: string,
  cause: string,
  cause_elaboration: string
) => {
  const token = localStorage.getItem("token");
  const role = localStorage.getItem("role") || "";

  const response = await fetch(
    `${BASE_API_URL}/report/report-tanggapan/${tanggapanId}/`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
        role: role,
      },
      body: JSON.stringify({ cause, cause_elaboration }),
      credentials: "include",
    }
  );

  if (response.status !== 201) {
    const data = await response.json();
    if(data.error_message){
      return data;
    }
    throw new Error(data);
  }

  return response.json();
};

export const getAllReportedPostingan = async () => {
  const token = localStorage.getItem("token");
  const role = localStorage.getItem("role") || "";
  const response = await fetch(`${BASE_API_URL}/report/reported-postingan/`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
      role: role,
    },
    credentials: "include",
  });

  if (response.status !== 200) {
    const data = await response.json();
    throw new Error(data);
  }

  return response.json();
};

export const getAllReportedTanggapan = async () => {
  const token = localStorage.getItem("token");
  const role = localStorage.getItem("role") || "";
  const response = await fetch(`${BASE_API_URL}/report/reported-tanggapan/`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
      role: role,
    },
    credentials: "include",
  });

  if (response.status !== 200) {
    const data = await response.json();
    throw new Error(data);
  }

  return response.json();
};

export const getSpecificReportedPostingan = async (postinganId: string) => {
  const token = localStorage.getItem("token");
  const role = localStorage.getItem("role") || "";
  const response = await fetch(
    `${BASE_API_URL}/report/reported-postingan/${postinganId}/`,
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
        role: role,
      },
      credentials: "include",
    }
  );

  if (response.status !== 200) {
    const data = await response.json();
    throw new Error(data);
  }

  return response.json();
};

export const getSpecificReportedTanggapan = async (tanggapanId: string) => {
  const token = localStorage.getItem("token");
  const role = localStorage.getItem("role") || "";
  const response = await fetch(
    `${BASE_API_URL}/report/reported-tanggapan/${tanggapanId}/`,
    {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
        role: role,
      },
      credentials: "include",
    }
  );

  if (response.status !== 200) {
    const data = await response.json();
    throw new Error(data);
  }

  return response.json();
};

export const takeDownPostingan = async (postinganId: string) => {
  const token = localStorage.getItem("token");
  const role = localStorage.getItem("role") || "";
  const response = await fetch(
    `${BASE_API_URL}/report/take-down-postingan/${postinganId}/`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
        role: role,
      },
      credentials: "include",
    }
  );

  if (response.status !== 200) {
    const data = await response.json();
    if (data.status === 400 && data.error_message){
      return data;
    }
    throw new Error(data);
  }

  return response.json();
};

export const takeDownTanggapan = async (tanggapanId: string) => {
  const token = localStorage.getItem("token");
  const role = localStorage.getItem("role") || "";
  const response = await fetch(
    `${BASE_API_URL}/report/report-postingan/${tanggapanId}/`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
        role: role,
      },
      credentials: "include",
    }
  );

  if (response.status !== 200) {
    const data = await response.json();
    if (data.status === 400 && data.error_message){
      return data;
    }
    throw new Error(data);
  }

  return response.json();
};

export const getAllTakenDownPostingan = async () => {
  const token = localStorage.getItem("token");
  const role = localStorage.getItem("role") || "";
  const response = await fetch(`${BASE_API_URL}/report/taken-down-postingan/`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
      role: role,
    },
    credentials: "include",
  });

  if (response.status !== 200) {
    const data = await response.json();
    throw new Error(data);
  }

  return response.json();
};

export const getAllTakenDownTanggapan = async () => {
  const token = localStorage.getItem("token");
  const role = localStorage.getItem("role") || "";
  const response = await fetch(`${BASE_API_URL}/report/taken-down-tanggapan/`, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
      role: role,
    },
    credentials: "include",
  });

  if (response.status !== 200) {
    const data = await response.json();
    throw new Error(data);
  }

  return response.json();
};

export const restoreTakenDownPostingan = async (postinganId: string) => {
  const token = localStorage.getItem("token");
  const role = localStorage.getItem("role") || "";
  const response = await fetch(
    `${BASE_API_URL}/report/restore-postingan/${postinganId}/`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
        role: role,
      },
      credentials: "include",
    }
  );

  if (response.status !== 200) {
    const data = await response.json();
    throw new Error(data);
  }

  return response.json();
};

export const restoreTakenDownTanggapan = async (tanggapanId: string) => {
  const token = localStorage.getItem("token");
  const role = localStorage.getItem("role") || "";
  const response = await fetch(
    `${BASE_API_URL}/report/restore-tanggapan/${tanggapanId}/`,
    {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
        role: role,
      },
      credentials: "include",
    }
  );

  if (response.status !== 200) {
    const data = await response.json();
    throw new Error(data);
  }

  return response.json();
};
